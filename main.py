from itertools import permutations
from copy import deepcopy

def create_cleaning_plan():
    plan = []
    flatmates = ['clara', '1', '2', '3', '4', '5']

    # check if permutation has enough(indicated by i)
    # new teams per job to reduce the plan to a min. cycel
    for i in range(3):
        for permutation in permutations(flatmates):
            if not valid_permutation(i, permutation, plan):
                continue
            plan.append(permutation)

    sorted_plan = sort_plan_for_max_alternation(plan)

    printer(sorted_plan)

def valid_permutation(requirement: int, permutation: list, plan: list):
    if not plan:
        return True

    bath = get_same_team_for_same_job(permutation, plan, 0, 1)
    kitchen = get_same_team_for_same_job(permutation, plan, 2, 3)
    garbage = get_same_team_for_same_job(permutation, plan, 4, 5)

    if not (bath and kitchen and garbage):
        if len(bath) + len(kitchen) + len(garbage) == requirement:
            return True
        else:
            return False
    else:
        return False

def get_same_team_for_same_job(permutation: list, plan: list, start: int, end: int):
    return [
            week for week in plan
            if permutation[start] in week[start:end + 1] and permutation[end] in week[start:end + 1]
        ]

def sort_plan_for_max_alternation(plan: list, sorted_plan: list = None, index: int = 0):
    plan_copy = deepcopy(plan)
    got_next_week = False
    if not sorted_plan:
        sorted_plan = []
        sorted_plan.append(plan[index])
        plan_copy.pop(index)
    else:
        for requirement in range(6): # try to get next element with highest possible alternation
            for i, week in enumerate(plan):
                if valid_sorting(requirement, sorted_plan[index - 1], week):
                    sorted_plan.append(plan[i])
                    plan_copy.pop(i)
                    got_next_week = True
                    break
            if got_next_week:
                break

    plan = plan_copy

    if plan:
        sorted_plan = sort_plan_for_max_alternation(plan, sorted_plan, index + 1)
    
    return sorted_plan

def valid_sorting(requirement: int, last_week: list, current_week: list):
    if int(current_week[0] in last_week[0:2]) \
            + int(current_week[1] in last_week[0:2]) \
            + int(current_week[2] in last_week[2:4]) \
            + int(current_week[3] in last_week[2:4]) \
            + int(current_week[4] in last_week[4:6]) \
            + int(current_week[5] in last_week[4:6]) == requirement:
            return True
    else:
        return False

def printer(plan: list):
    with open('plan.md', 'w') as f:
        f.write('Woche | Bad | Küche | Müll \n')
        f.write('---|---|---|--- \n')
        for i, week in enumerate(plan, 1):
            f.write(f'{i} | {week[0]}, {week[1]} | {week[2]}, {week[3]} | {week[4]}, {week[5]} \n')
        
if __name__ == '__main__':
    create_cleaning_plan()